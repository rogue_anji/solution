Features:
- List of posts with username displayed + detail page
- Handle success, progress and error states
- Dependency Injection
- Custom Drawables for CardView
- Espresso UI tests
- Mockito Unit Test

Uses:

Kotlin
Dagger
Lifeycle components
RecyclerView
RxJava
Retrofit
JUnit

Build Instructions :

Android Studio
- Open the unzipped solution in Android Studio
- Run the project
- For UI testing, go to the androidTest directory and right click to run "All tests"
- For unit testing, go to the test directory and right click to run "All tests"

Command Line
- Unzip the solution
- cd to the directory (Make sure you see "gradlew" in this directory)
- add the android sdk location by adding "local.properties" file at this location
- run the project from the terminal "./gradlew installDebug".
- UI test the project "./gradlew connectedAndroidTest".
- Unit  the project "./gradlew test".

