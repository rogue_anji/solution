package com.anjali.solution

import android.content.Context
import android.os.Looper
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.anjali.solution.model.PostDao
import com.anjali.solution.model.PostItemModel
import com.anjali.solution.network.Resource
import com.anjali.solution.network.api.NetworkApi
import com.anjali.solution.network.repository.HomeRepositoryImpl
import com.anjali.solution.network.repository.IHomeRepository
import com.anjali.solution.viewmodel.MainViewModel
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeActivityUnitTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    @Mock
    lateinit var api : NetworkApi

    @Mock
    lateinit var postDao : PostDao

    @Mock
    lateinit var repository: IHomeRepository
    lateinit var viewModel: MainViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = HomeRepositoryImpl(api, postDao)
        viewModel = MainViewModel(repository)
        insertPosts()
    }
    val posts = ArrayList<PostItemModel>()

    private fun insertPosts() {
        posts.add(
            PostItemModel(
            1, "Title1", "Body1", "Name1"
        )
        )
        posts.add(PostItemModel(
            2, "Title2", "Body2", "Name2"
        ))

    }

    @Test
    fun testPostSize() {
        val data = MutableLiveData<List<PostItemModel>>()
        viewModel.viewState.postValue( MainViewModel.ViewState(showProgress = true,
            showError= false,
            showContent = false,
            errorText = null))

        data.postValue(posts)
        Mockito.`when`(repository.getPosts()).thenReturn(data)

        Assert.assertTrue(viewModel.getOfflinePosts().value?.size == 2)
        Assert.assertNotNull(viewModel.getOfflinePosts().value)
        Assert.assertNotEquals(viewModel.getOfflinePosts().value?.size, 4)
    }

    @Test
    fun testPostDetail() {
        val data = MutableLiveData<List<PostItemModel>>()
        data.postValue(posts)
        Mockito.`when`(repository.getPosts()).thenReturn(data)

        Assert.assertEquals(viewModel.getOfflinePosts().value?.get(0)?.username, "Name1")
        Assert.assertTrue(viewModel.getOfflinePosts().value?.get(0)?.title == "Title1")
        Assert.assertFalse(viewModel.getOfflinePosts().value?.get(0)?.username == "Name2")
    }
}