package com.anjali.solution.di

import com.anjali.solution.MainApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = arrayOf(
    NetworkModule::class))
class AppModule {

    @Provides
    @Singleton
    fun application(): MainApplication {
        return MainApplication()
    }

}
