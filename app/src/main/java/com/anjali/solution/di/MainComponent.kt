package com.anjali.solution.di;

import android.content.Context
import com.anjali.solution.MainApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityModule::class,
    FragmentModule::class,
    AppModule::class,
    DataModule::class])

public interface MainComponent {

    fun inject(app : MainApplication)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: MainApplication): Builder

        @BindsInstance
        fun context(context: Context): Builder

        fun build(): MainComponent
    }
}