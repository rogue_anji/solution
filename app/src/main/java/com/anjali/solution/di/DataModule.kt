package com.anjali.solution.di;

import android.content.Context
import androidx.room.Room
import com.anjali.solution.model.PostDB
import com.anjali.solution.model.PostDao
import com.anjali.solution.network.api.NetworkApi
import com.anjali.solution.network.repository.IHomeRepository
import com.anjali.solution.network.repository.HomeRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun providesAppDatabase(context: Context) : PostDB {
        return Room.databaseBuilder(
            context, PostDB::class.java, "database")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()

    }

    @Provides
    @Singleton
    fun providesPostsDAO(database: PostDB) : PostDao {
        return database.postsDao()
    }

    @Singleton
    @Provides
    fun providesHomeRepository(api : NetworkApi, postDao : PostDao) : IHomeRepository = HomeRepositoryImpl(api, postDao)


}