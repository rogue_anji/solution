package com.anjali.solution.network.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.http.GET
import rx.Observable

interface NetworkApi {

    @GET("posts")
    fun fetchPosts() : Observable<JsonElement>

    @GET("users")
    fun fetchUsers() : Observable<JsonElement>
}