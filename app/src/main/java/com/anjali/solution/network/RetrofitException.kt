package com.anjali.solution.network

import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class RetrofitException(
    message: String?,
    /** The request URL which produced the error.  */
    val url: String?,
    /** Response object containing status code, headers, body, etc.  */
    val response: Response<*>?,
    /** The event kind which triggered this error.  */
    val kind: Kind,
    exception: Throwable?,
    /** The Retrofit this request was executed on  */
    val retrofit: Retrofit?
) : Exception(message, exception) {

    /** Identifies the event kind which triggered a [RetrofitException].  */
    enum class Kind {
        /** An [IOException] occurred while communicating to the server.  */
        NETWORK,

        /** A non-200 HTTP status code was received from the server.  */
        HTTP,

        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    companion object {
        fun httpError(
            url: String,
            response: Response<*>,
            retrofit: Retrofit
        ): RetrofitException {
            val message = response.code().toString() + " " + response.message()
            return RetrofitException(
                message, url, response,
                Kind.HTTP, null, retrofit
            )
        }

        fun networkError(exception: IOException): RetrofitException {
            return RetrofitException(
                exception.message ?: "Unknown Error", null, null,
                Kind.NETWORK, exception,
                null
            )
        }

        fun unexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(
                exception.message ?: "Unknown Error", null, null,
                Kind.UNEXPECTED,
                exception, null
            )
        }
    }
}