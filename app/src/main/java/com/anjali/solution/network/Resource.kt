package com.anjali.solution.network

import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class Resource<T> (val status: Resource.Status, val data: T?, val exception: RetrofitException?
                   , val error : AppException?) {
    enum class Status {
        SUCCESS, RETROFIT_ERROR, ERROR
    }

    companion object {

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null, null)
        }

        fun <T> exception(exception: RetrofitException?): Resource<T> {
            return Resource(Status.RETROFIT_ERROR, null, exception, null)
        }

        fun <T> error(exception: AppException?): Resource<T> {
            return Resource(Status.ERROR, null, null, exception)
        }
    }

    class AppException(val exception: Throwable?) : Exception()
}