package com.anjali.solution.common.ui

import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.anjali.solution.MainApplication
import com.anjali.solution.R
import com.anjali.solution.network.Resource
import com.anjali.solution.network.RetrofitException

abstract class BaseViewModel : ViewModel() {
    val networkObservable: MutableLiveData<Void> = MutableLiveData()

    fun getErrorMessage(error: Resource.AppException?): String? {
        return error?.localizedMessage
    }

    fun getExceptionMessage(exception: RetrofitException?): String {
        var errorMessage = ""
        try {
            val error = exception as RetrofitException
            if (error?.kind == RetrofitException.Kind.HTTP) {
                // parsing error json to pojo
                errorMessage = MainApplication.getInstance().applicationContext?.getString(
                    R.string.http_error)!!
                Log.e("Error response %s", errorMessage)
            } else if (error?.kind == RetrofitException.Kind.NETWORK) {
                Log.e("Error response %s", error.localizedMessage)
                errorMessage = MainApplication.getInstance().applicationContext?.getString(R.string.please_check_your_connection)!!
            }
        } catch (e1: Exception) {
            e1.printStackTrace()
            errorMessage = e1.localizedMessage
        }

        return errorMessage
    }
}