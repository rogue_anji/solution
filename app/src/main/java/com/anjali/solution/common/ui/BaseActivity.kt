package com.anjali.solution.common.ui

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.anjali.solution.R
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Abstract Base Class for all activities
 */
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResource)

        if (needToolbar) {
            val currToolbar = this.findViewById<Toolbar>(R.id.toolbar)

            setSupportActionBar(currToolbar)
            supportActionBar?.let { actionBar ->
                if (homeUpEnabled) {
                    actionBar.setDisplayHomeAsUpEnabled(true)
                }

                actionBar.title = toolbarText
            }
        }
    }

    protected abstract val layoutResource: Int

    protected abstract val homeUpEnabled: Boolean

    protected abstract val needToolbar: Boolean

    protected abstract val toolbarText: String

    protected fun setToolbarText(text: String) {
        supportActionBar?.title = text //Clearing default title
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (homeUpEnabled && id == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

}