package com.anjali.solution

import android.app.Activity
import android.app.Application
import com.anjali.solution.di.DaggerMainComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MainApplication : Application(), HasActivityInjector {

    companion object {

        lateinit var appInstance: MainApplication

        fun getInstance(): MainApplication {
            return appInstance
        }
    }

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>
    override fun onCreate() {
        super.onCreate()
        appInstance = this
        DaggerMainComponent
            .builder()
            .application(this)
            .context(applicationContext)
            .build()
            .inject(this)
    }


    override fun activityInjector(): DispatchingAndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }
}