package com.anjali.solution.posts.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.anjali.solution.common.ui.BaseViewModel
import com.anjali.solution.posts.model.PostItemModel
import com.anjali.solution.network.Resource
import com.anjali.solution.posts.repository.IPostRepository
import com.anjali.solution.utils.SingleLiveEvent
import javax.inject.Inject
import android.os.AsyncTask


/***
 * MainViewModel responsible to fetch data from repo and update the UI
 */
class MainViewModel
@Inject
constructor(
    private val repository: IPostRepository
) : BaseViewModel() {

    var posts: List<PostItemModel>? = null

    private lateinit var existingPostsLiveData : LiveData<List<PostItemModel>>

    val viewState = MutableLiveData<ViewState>()
    val command = SingleLiveEvent<Command>()
    private var postsSwitchLiveData: LiveData<Resource<List<PostItemModel>>>? =
        Transformations.switchMap(networkObservable) {
            repository.fetchPosts()
        }

    data class ViewState(
        var showProgress: Boolean = true,
        var showError: Boolean = false,
        var showContent: Boolean = false,
        var errorText : String? = null
    )

    sealed class Command{
        class DisplayPosts(var model : List<PostItemModel>?): Command()
    }

    init {
        reset()
    }

    fun reset() {
        networkObservable.value = null
        viewState.value = ViewState()
    }

    fun getPostsLiveData() : LiveData<Resource<List<PostItemModel>>>?{
        return postsSwitchLiveData
    }

    fun getOfflinePosts() : LiveData<List<PostItemModel>>{
        existingPostsLiveData = repository.getPosts()
        return existingPostsLiveData
    }

    fun savePosts(data: List<PostItemModel>?) {
        data?.let {
            PopulateDbAsync(repository).execute(it)
        }
    }

    private class PopulateDbAsync internal constructor(
        private val repository: IPostRepository) :
        AsyncTask<List<PostItemModel>, Void, Void>() {

        override fun doInBackground(vararg p0: List<PostItemModel>): Void? {
            repository.deletePosts()
            repository.savePosts(p0[0])
            return null
        }
    }
}