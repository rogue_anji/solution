package com.anjali.solution.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.anjali.solution.common.ui.BaseActivity
import com.anjali.solution.model.PostItemModel
import com.anjali.solution.view.adapter.HomeAdapter
import com.anjali.solution.R
import com.anjali.solution.common.ui.BaseOnItemClickListener
import com.anjali.solution.view.activity.PostDetailActivity
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : AppFragment(), BaseOnItemClickListener {
    override fun onItemClick(position: Int) {
        val post: PostItemModel = homeAdapter.items[position]
        val intent = Intent(activity, PostDetailActivity::class.java)
        intent.putExtra(PostDetailActivity.KEY_POST, post)
        startActivity(intent)
    }

    companion object {
        fun newInstance() = HomeFragment()
    }

    lateinit var homeAdapter: HomeAdapter

    fun refreshData(posts : List<PostItemModel>?) {
        posts?.let {
          homeAdapter?.items = ArrayList(posts)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rvList?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        homeAdapter = HomeAdapter(activity as BaseActivity)
        homeAdapter.setOnItemClickListener(this)
        rvList?.adapter = homeAdapter
        this.context?.let { ctx ->
            rvList?.addItemDecoration(homeAdapter.ViewItemDecoration(ctx))
        }
    }
}