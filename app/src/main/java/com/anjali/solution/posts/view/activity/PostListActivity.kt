package com.anjali.solution.view.activity

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.anjali.solution.R
import com.anjali.solution.common.ui.BaseActivity
import com.anjali.solution.model.PostItemModel
import com.anjali.solution.network.Resource
import com.anjali.solution.view.fragment.HomeFragment
import com.anjali.solution.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*

import javax.inject.Inject
import kotlin.collections.ArrayList

/***
 * HomeActivity that holds the fragment with the recyclerview
 */
class HomeActivity : BaseActivity() , HasSupportFragmentInjector {

    private lateinit var fragment: HomeFragment
    @Inject lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): DispatchingAndroidInjector<Fragment> {
        return dispatchingAndroidInjector
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel : MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AndroidInjection.inject(this)
        fragment = HomeFragment.newInstance()
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .commitAllowingStateLoss()
        }

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)

        errorView.setOnRetryListener { viewModel.reset() }

        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.getOfflinePosts()?.observe(this,
            Observer<List<PostItemModel>> { posts ->
                posts?.let {
                    if(it.isNotEmpty()){
                        viewModel.posts = it
                        viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = false, errorText = null, showContent = true)
                        viewModel.command.value = MainViewModel.Command.DisplayPosts(ArrayList(it))
                    }
                }
            })

        viewModel.getPostsLiveData()?.observe(this,
            Observer<Resource<List<PostItemModel>>> { resource ->
                if (resource != null) {
                    when (resource.status) {
                        Resource.Status.SUCCESS -> {
                            viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = false, errorText = null, showContent = true)
                            viewModel.command.value = MainViewModel.Command.DisplayPosts(resource.data)
                            viewModel.savePosts(resource.data)
                        }

                        Resource.Status.RETROFIT_ERROR -> {
                            if(viewModel?.posts?.isNullOrEmpty() == false) {
                                viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = false, errorText = null, showContent = true)
                            } else {
                                viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = true, errorText = viewModel.getExceptionMessage(resource.exception), showContent = false)
                            }
                        }

                        Resource.Status.ERROR -> {
                            if(viewModel?.posts?.isNullOrEmpty() == false) {
                                viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = false, errorText = null, showContent = true)
                            } else {
                                viewModel.viewState.value = viewModel.viewState.value?.copy(showProgress = false, showError = true, errorText = viewModel.getErrorMessage(resource.error), showContent = false)
                            }
                        }
                    }
                }
            })

        viewModel.viewState.observe(this, Observer {viewState ->
            viewState?.let {
                render(it)
            }
        })

        viewModel.command.observe(this, Observer {command ->
            command?.let {
                listenToCommand(it)
            }

        })

    }

    private fun listenToCommand(command: MainViewModel.Command) {
        when(command) {
            is MainViewModel.Command.DisplayPosts -> {
                fragment.refreshData(command.model)
            }
        }
    }

    private fun render(viewState: MainViewModel.ViewState) {
        errorView.visibility = if(viewState.showError) View.VISIBLE else View.GONE
        progressBar.visibility = if(viewState.showProgress) View.VISIBLE else View.GONE
        contentView.visibility = if(viewState.showContent) View.VISIBLE else View.GONE
        errorView.subtitle = viewState?.errorText
    }

    override val layoutResource: Int = R.layout.activity_main
    override val homeUpEnabled: Boolean = false
    override val needToolbar: Boolean = true
    override val toolbarText: String = "Solution"
}
