package com.anjali.solution.view.activity

import android.os.Bundle
import android.view.MenuItem
import com.anjali.solution.R
import com.anjali.solution.common.ui.BaseActivity
import com.anjali.solution.model.PostItemModel
import kotlinx.android.synthetic.main.activity_post_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class PostDetailActivity : BaseActivity() {
    companion object {
        val KEY_POST = "key_post"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        intent.getSerializableExtra(KEY_POST)?.let {
            var postData = it as? PostItemModel
            updateView(postData)
        }
    }

    private fun updateView(postData: PostItemModel?) {
        tvPostTitle.text = postData?.title
        tvPostBody.text = postData?.postBody
        tvUsername.text = postData?.username
    }

    override val layoutResource: Int = R.layout.activity_post_detail
    override val homeUpEnabled: Boolean = true
    override val needToolbar: Boolean = true
    override val toolbarText: String = "Detail"


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
