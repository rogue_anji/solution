package com.anjali.solution.network.repository

import androidx.lifecycle.LiveData
import com.anjali.solution.model.PostItemModel
import com.anjali.solution.network.Resource

interface IHomeRepository {

    fun savePosts(posts : List<PostItemModel>)

    fun getPosts(): LiveData<List<PostItemModel>>

    fun deletePosts()

    fun fetchPosts(): LiveData<Resource<List<PostItemModel>>>
}