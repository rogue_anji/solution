package com.anjali.solution.view.adapter

import android.content.Context
import android.graphics.Rect
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anjali.solution.common.ui.BaseActivity
import com.anjali.solution.common.ui.BaseAdapter
import com.anjali.solution.model.PostItemModel
import com.anjali.solution.R
import com.anjali.solution.common.ui.BaseOnItemClickListener
import kotlinx.android.synthetic.main.home_item.view.*

/***
 * HomeAdapter is responsible for transaction item layout and binding of the data
 */
class HomeAdapter(val mActivity : BaseActivity) : BaseAdapter<ItemViewHolder, PostItemModel>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val holder = ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.home_adapter_item, parent, false),
            mOnItemClickListener)

        return holder
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val option = mItems[position]
        holder.bind(option)
    }

    //Item Decorator for Adapter
    inner class ViewItemDecoration(val mContext: Context) : RecyclerView.ItemDecoration() {
        private var space = 0

        init {
            space = mContext.resources.getDimensionPixelOffset(R.dimen.fab_margin)
        }

        override fun getItemOffsets(
            outRect: Rect, view: View, parent: RecyclerView,
            state: RecyclerView.State
        ) {
            if (parent.getChildAdapterPosition(view) == 0) outRect.top = space
            val position = parent.getChildAdapterPosition(view)
            if (position == -1) return

            if (position == items.size - 1) {
                outRect.bottom = space
            }
        }
    }

}

class ItemViewHolder(
    itemView: View,
    mOnItemClickListener: BaseOnItemClickListener
) : RecyclerView.ViewHolder(itemView)  {

    init {
        this.itemView.setOnClickListener {
            val position = adapterPosition
            if (position == -1) return@setOnClickListener
            mOnItemClickListener?.onItemClick(position)
        }
    }

    fun bind( displayItem: PostItemModel?) {
        displayItem?.let { d ->

            itemView?.tvUsername.text = d.username
            itemView?.tvPost.text = d.title
        }

    }
}