package com.anjali.solution.model

import androidx.room.Database
import androidx.room.RoomDatabase


@Database( entities = arrayOf(PostItemModel::class), version = 2 , exportSchema = false)
abstract class PostDB : RoomDatabase() {
    abstract fun postsDao(): PostDao
}