package com.anjali.solution.model

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("id") val id : Int,
    @SerializedName("username") val userName : String,
    @SerializedName("name") val name : String
)

data class IndividualPost(
    @SerializedName("userId") val userId : Int,
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String,
    @SerializedName("body") val body : String
)
