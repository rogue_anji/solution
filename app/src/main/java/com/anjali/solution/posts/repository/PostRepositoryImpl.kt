package com.anjali.solution.network.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.anjali.solution.model.*
import com.anjali.solution.network.Resource
import com.anjali.solution.network.RetrofitException
import com.anjali.solution.network.api.NetworkApi
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepositoryImpl
    @Inject
    constructor(
        val homeApi: NetworkApi,
        val postDao : PostDao
    ) : IHomeRepository {
    override fun deletePosts() {
        postDao.deletePosts()
    }

    override fun getPosts(): LiveData<List<PostItemModel>> {
        return postDao.getPosts()
    }

    override fun savePosts(posts: List<PostItemModel>) {
        postDao.savePosts(posts)
    }

    override fun fetchPosts(): LiveData<Resource<List<PostItemModel>>> {

        val data = MutableLiveData<Resource<List<PostItemModel>>>()

        Observable.combineLatest(
            homeApi.fetchPosts(),
            homeApi.fetchUsers()
        ){ post, user ->
            val obj = JsonObject()
            if (!post.isJsonNull) {
                obj.add("posts", post)
            }
            if (!user.isJsonNull) {
                obj.add("users", user)
            }
            return@combineLatest obj
        }
            ?.map { data ->
                val typePost = object : TypeToken<ArrayList<IndividualPost>>() {}.type
                val posts = Gson().fromJson<ArrayList<IndividualPost>>(
                    data.get("posts"), typePost)

                val typeUser = object : TypeToken<ArrayList<UserModel>>() {}.type
                val users = Gson().fromJson<ArrayList<UserModel>>(
                    data.get("users"), typeUser)

                val items = ArrayList<PostItemModel>()
                posts?.forEachIndexed { index, post ->
                    if(index > 20) return@forEachIndexed
                    items.add(
                        PostItemModel(
                            id = post.id,
                            title = post.title,
                            username = "- " + users?.firstOrNull {
                                return@firstOrNull (it.id == post.userId)
                            }?.userName,
                            postBody = post.body
                        ))
                }
                return@map items.toList()
            }?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.newThread())
                ?.subscribe( object : Subscriber<List<PostItemModel>>() {

                    override fun onCompleted() {

                    }

                    override fun onNext(t: List<PostItemModel>) {
                        data.value = Resource.success(t)
                    }

                    override fun onError(e: Throwable) {

                        if (e is RetrofitException) {
                            data.setValue(Resource.exception(e))
                        } else {
                            data.setValue(Resource.error(e as Resource.AppException))
                        }
                    }
                }
                )

        return data
    }
}