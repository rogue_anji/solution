package com.anjali.solution.model

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface PostDao {

    @Insert( onConflict = OnConflictStrategy.REPLACE )
    fun savePosts(w: List<PostItemModel>)

    @Query("DELETE FROM posts")
    fun deletePosts()

    @Query( "SELECT * FROM posts")
    fun getPosts(): LiveData<List<PostItemModel>>
}