package com.anjali.solution.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity( tableName = "posts" )
data class PostItemModel(
    @PrimaryKey
    val id : Int,
    val title: String?,
    val postBody : String?,
    var username : String?
) : Serializable