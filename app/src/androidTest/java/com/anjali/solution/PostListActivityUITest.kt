package com.anjali.solution

import androidx.test.espresso.Espresso
import androidx.test.rule.ActivityTestRule
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.anjali.solution.view.activity.HomeActivity
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4


@androidx.test.filters.LargeTest
@RunWith(AndroidJUnit4::class)
public class HomeActivityUITest {

    @Rule @JvmField
    public val mActivityTestRule: ActivityTestRule<HomeActivity> =
        ActivityTestRule(HomeActivity::class.java)

    @Test
    fun checkContentVisibilty() {
        Espresso.onView(ViewMatchers.withId(R.id.contentView))
            .check(
                ViewAssertions.matches(
                ViewMatchers.withEffectiveVisibility(
                    ViewMatchers.Visibility.VISIBLE)))

    }

    @Test
    fun checkProgressVisibility() {
        Espresso.onView(ViewMatchers.withId(R.id.progressBar))
            .check(
                ViewAssertions.matches(
                    ViewMatchers.withEffectiveVisibility(
                        ViewMatchers.Visibility.GONE)))

    }

    @Test
    fun checkErrorInVisibilty() {
        Espresso.onView(ViewMatchers.withId(R.id.errorView))
            .check(
                ViewAssertions.matches(
                    ViewMatchers.withEffectiveVisibility(
                        ViewMatchers.Visibility.GONE)))

    }
}